<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\Http\Requests\ArticlesFormRequest;
class ArticlesController extends Controller
{
    public function index(){

        $articles = Article::paginate(5);

        return view("articles.index", compact('articles'));
    }

    public function show($id){
        $articles = Article::find($id);
        return view('articles.show')->with('article',$articles);
    }
    
    public function create(){
    	return view('articles.create');
    }

    public function store(ArticlesFormRequest $requests){
    	$title = $requests->input('title');
    	$content = $requests->input('content');

    	Article::create([
    			'title' => $title,
    			'content' => $content
    		]);
    	return redirect()->route('articles');
    }

    public function edit($id){
        $article = Article::find($id); 

        return view('articles.edit',compact('article'));
    }

    public function update($id,ArticlesFormRequest $requests){
        $article = Article::find($id);

        $article->update([
            'title' => $requests->get('title'),
            'content' => $requests->get('content')
            ]);
        return redirect()-> route('articles');
    }

    public function destroy($id){
        $article = Article::find($id);

        $article->delete();

        return redirect()-> route('articles');
    }
}
