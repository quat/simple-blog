<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('head.title')</title>
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	@yield('head.css')
</head>
<body>
	@include('partials.navbar')
	@yield('body.content')
	<script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	@yield('body.js')
</body>
</html>