@extends('layouts.master')
@section('head.title')
home	
@endsection
@section('body.content')
	<div class="container">
		@foreach($articles as $a)
		<div class="col-sm-6 col-sm-offset-3">
			<h2>{{ $a->title }}</h2>
			<p>
				{{ $a-> content }}
			</p>
			<a href="{{ route('articles.show',$a->id)  }}">Read more</a>
		</div>
		@endforeach
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				{!! $articles->render() !!}
			</div>
		</div>
	</div>
@endsection
