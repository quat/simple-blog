				<div class="form-group">
					{!! Form::label('title','Title',['class' => 'control-label']) !!}
					{!! Form::text('title',null,['id'=> 'title', 'class' => 'form-control','placeholder' => 'Title','required' => 'true']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('content','Content',['class' => 'control-label']) !!}
					{!! Form::text('content',null,['id'=> 'content', 'class' => 'form-control','placeholder' => 'Content','required' =>'true']) !!}
				</div>
				<div class="form-group">
					{!! Form::submit($btn,['class'=> 'btn btn-primary' ])!!}
				</div>