@extends('layouts.master')
@section('head.title')
them bai viet  moi	
@endsection
@section('body.content')
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<h2>Add new article</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">

				@if(count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				{!! Form::open([
					'route' => ['articles.store'],
					'method' => 'POST',
					'class' => 'form-horizontal'
				]) !!}

				@include('articles._form',[	'btn' => 'Create'])

				{!! Form::close()!!}
			</div>
		</div>
	</div>
@endsection
