@extends('layouts.master')
@section('head.title')
	tieu de
@endsection
@section('body.content')
	<div class="container">
	    <div class="row">
	        <div class="col-sm-6 col-sm-offset-3">
	            <a class="btn btn-link" href="{{ url('/') }}">
	                <span class="glyphicon glyphicon-chevron-left">
	                    Back to home
	                </span>
	            </a>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-sm-6 col-sm-offset-3">
        			<h2>{{ $article->title }}</h2>
        			<p>
        			{{  $article->content }}
        			</p>
        	</div>
	    </div>
	    <div class="row">
	    	<div class="col-sm-6 col-sm-offset-3">
	    		<a href="{{ route('articles.edit',$article->id) }}" class="btn btn-info">Edit</a>
	    	{!! Form::open([
	    			'route' => ['articles.destroy', $article->id ],
	    			'method' => 'delete',
	    			'style' => 'display: inline'
	    		])
	    	!!}
			<button class="btn btn-danger">Delete</button>
	    	{!! Form::close() !!}
	    	</div>
	    </div>
	</div>
@endsection
